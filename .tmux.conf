# Reload tmux config
bind r source-file ~/.tmux.conf \; display "Reloaded tmux config"

# Mouse mode
set -g mouse on

# Don't exit when leaving a session
set-option -g detach-on-destroy off

# Start windows and panes at 1, not 0
set -g base-index 1
set -g pane-base-index 1
setw -g pane-base-index 1

# Vi navigation in copy mode
setw -g mode-keys vi

# Split windows easily
bind | split-window -h # horizontal columns
bind - split-window -v # vertical rows

# Vi style pane navigation
bind h select-pane -L
bind j select-pane -D
bind k select-pane -U
bind l select-pane -R

bind -n C-h select-pane -L
bind -n C-j select-pane -D
bind -n C-k select-pane -U
bind -n C-l select-pane -R

# Resizing panes
bind -r H resize-pane -L 5
bind -r J resize-pane -D 5
bind -r K resize-pane -U 5
bind -r L resize-pane -R 5

# Allow for clearing screen
bind C-l send-keys 'C-l'

set -g default-terminal "xterm-256color"

# Vim - tmux navigator
is_vim='echo "#{pane_current_command}" | grep -iqE "(^|\/)g?(view|n?vim?)(diff)?$"'
bind -n C-h if-shell "$is_vim" "send-keys C-h" "select-pane -L"
bind -n C-j if-shell "$is_vim" "send-keys C-j" "select-pane -D"
bind -n C-k if-shell "$is_vim" "send-keys C-k" "select-pane -U"
bind -n C-l if-shell "$is_vim" "send-keys C-l" "select-pane -R"
bind -n C-\ if-shell "$is_vim" "send-keys C-\\" "select-pane -l"

# List of plugins
set -g @plugin 'tmux-plugins/tpm'
set -g @plugin 'tmux-plugins/tmux-sensible'
set -g @plugin 'tmux-plugins/tmux-resurrect'
set -g @plugin 'tmux-plugins/tmux-yank'
set -g @plugin 'tmux-plugins/tmux-copycat'
set -g @plugin 'tmux-plugins/tmux-continuum'
set -g @plugin 'tmux-plugins/tmux-sessionist'
set -g @plugin 'tmux-plugins/tmux-prefix-highlight'
set -g @plugin 'tmux-plugins/tmux-logging'
set -g @plugin 'Morantron/tmux-fingers'

set -g @continuum-restore 'on'
set -g @resurrect-capture-pane-contents 'on'

set -g history-limit 100000

# Add prefix to right status bar
set -g status-left '#[bg=#009ddc,fg=#ffffff] ❐ #S '
set -g status-right '#{prefix_highlight} | "#{=21:pane_title}" %Y-%m-%d %H:%M'

run-shell "powerline-daemon -q"
source "/home/ben/.local/lib/python3.6/site-packages/powerline/bindings/tmux/powerline.conf"

# Initialize TMUX plugin manager (keep this line at the very bottom of tmux.conf)
run '~/.tmux/plugins/tpm/tpm'

