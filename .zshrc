# Path to your oh-my-zsh installation.
export ZSH=/home/ben/.oh-my-zsh

# Set name of the theme to load. Optionally, if you set this to "random"
# it'll load a random theme each time that oh-my-zsh is loaded.
# See https://github.com/robbyrussell/oh-my-zsh/wiki/Themes
ZSH_THEME="robbyrussell"

# Set list of themes to load
# Setting this variable when ZSH_THEME=random
# cause zsh load theme from this variable instead of
# looking in ~/.oh-my-zsh/themes/
# An empty array have no effect
# ZSH_THEME_RANDOM_CANDIDATES=( "robbyrussell" "agnoster" )

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion. Case
# sensitive completion must be off. _ and - will be interchangeable.
HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(
  git
  omz-git
  vi-mode
  kubectl
)

source $ZSH/oh-my-zsh.sh

# User configuration

fpath+=~/.zfunc

# Paths

# Rust related
export PATH=$PATH:$HOME/.local/bin:$HOME/.cargo/bin
export RUST_SRC_PATH="$(rustc --print sysroot)/lib/rustlib/src/rust/src"

# Gopath
export GOPATH=$HOME/go
export PATH="$PATH:$GOPATH/bin"

# Manually set language environment
export LANG=en_GB.UTF-8

# Increase history size
HISTFILE=~/.zhistory
HISTSIZE=SAVEHIST=10000
setopt sharehistory
setopt extendedhistory

# This gets a directory history
DIRSTACKSIZE=20
setopt autopushd pushdminus pushdsilent pushdtohome
alias dh='dirs -v'

export EDITOR=vim
set -o vi
export TERM=xterm-256color

############################

## Aliases
alias vim='vim -X'
alias ta='tmux attach'
alias bat=cat

# Locking / sleeping
alias lock='i3lock -c 000000'
alias susp='sudo systemctl suspend'

# iPython/Jupyter
alias ipython=ptipython
alias jtheme='jt -f ubuntu -nf robotosans -tf robotosans -t chesterish -vim -T -ofs 10'

# git aliases
alias gs='git status'
alias gc='git commit -v'
alias gcv='git commit -v --no-verify'
alias gd='git diff'
alias gb='git branch'
alias ga='git add'
alias goo='git checkout'

# kubernetes aliases
alias kctl='kubectl'
alias kget='kubectl get'
alias kg='kubectl get'
alias kapp='kubectl apply'
alias kdel='kubectl delete'
alias kdes='kubectl describe'
alias kpods='kubectl get pods'
alias kdepl='kubectl get deployments'
alias ksvc='kubectl get svc'
alias ktop='kubectl top'
alias kcontext='kubectl config use-context'
alias kns='kubectl config set-context $(kubectl config current-context) --namespace'
alias ktail='stern -t -s 1s'
alias kedit-secret="KUBE_EDITOR=kube-secret-editor kubectl edit secret"
source <(kubectl completion zsh)

# ls aliases
alias lh='ls -lh'

# task aliases
alias pt='task project:personal'
alias pta='pt add'

# Use vi bindings
bindkey -v
bindkey '^P' up-history
bindkey '^N' down-history
bindkey '^?' backward-delete-char
bindkey '^h' backward-delete-char
bindkey '^w' backward-kill-word
bindkey '^r' history-incremental-search-backward
bindkey "^[[1~" vi-beginning-of-line   # Home
bindkey "^[[4~" vi-end-of-line         # End
bindkey '^[[2~' beep                   # Insert
bindkey '^[[3~' delete-char            # Del
bindkey '^[[5~' vi-backward-blank-word # Page Up
bindkey '^[[6~' vi-forward-blank-word  # Page Down
autoload edit-command-line
zle -N edit-command-line
bindkey -M vicmd v edit-command-line
function zle-line-init zle-keymap-select {
    VIM_PROMPT="%{$fg_bold[yellow]%} [% NORMAL]%  %{$reset_color%}"
    zle reset-prompt
}
zle -N zle-line-init
zle -N zle-keymap-select
export KEYTIMEOUT=1

###################

# Virtualenvwrapper
export WORKON_HOME=$HOME/.virtualenvs
export PROJECT_HOME=$HOME/repos
export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python
export VIRTUALENVWRAPPER_VIRTUALENV=/usr/bin/virtualenv
export VIRTUALENVWRAPPER_SCRIPT=$HOME/.local/bin/virtualenvwrapper.sh
source $HOME/.local/bin/virtualenvwrapper_lazy.sh

# Conda environments
function useconda {
    export USINGCONDA=3
    export OLDPATH=$PATH
    export OLDPYTHONPATH=$PYTHONPATH
    export PATH=$HOME/.anaconda3/bin:$PATH
    export PYTHONPATH=$HOME/.anaconda3
}
function useconda2 {
    export USINGCONDA=2
    export OLDPATH=$PATH
    export OLDPYTHONPATH=$PYTHONPATH
    export PATH=$HOME/.anaconda2/bin:$PATH
    export PYTHONPATH=$HOME/.anaconda2
}
function screwconda {
    if [ "$USINGCONDA" ]
        then
            export PATH=$OLDPATH
            export PYTHONPATH=$OLDPYTHONPATH
            unset USINGCONDA
            unset OLDPATH
            unset OLDPYTHONPATH
    fi
}

# Notetaking function
function nn () {
    NOTE_DIR="${HOME}/.notes"
    TEMPLATE_FILE="${NOTE_DIR}/.template.md"
    NOTE_NAME="${1:-notes}"
    CURRENT_DATE="$(date +%Y-%m-%d)"
    DATE="${2:-$CURRENT_DATE}"
    NOTE_FILE="${NOTE_DIR}/${DATE}-${NOTE_NAME}.md"
    [ -f "${NOTE_FILE}" ] || cp ${TEMPLATE_FILE} ${NOTE_FILE} && sed -i "s/DATE/${DATE}/g" ${NOTE_FILE}
    vim "${NOTE_FILE}"
}

# Fuzzy find (fzf) options
export FZF_CTRL_T_OPTS="--preview '(highlight -O ansi -l {} 2> /dev/null || cat {} || tree -C {}) 2> /dev/null | head -200' --select-1 --exit-0"
export FZF_CTRL_R_OPTS="--preview 'echo {}' --preview-window down:3:hidden:wrap --bind '?:toggle-preview'"
export FZF_ALT_C_OPTS="--preview 'tree -C {} | head -200' --select-1 --exit-0"
[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
[ -f ~/.fzf-git.zsh ] && source ~/.fzf-git.zsh
source /usr/share/fzf/completion.zsh
source /usr/share/fzf/key-bindings.zsh
