set nocompatible              " be iMproved, required
filetype off                  " required
set t_Co=256
set incsearch
set scrolloff=1
set ruler

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
" call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'gmarik/Vundle.vim'

" The following are examples of different formats supported.
" Keep Plugin commands between vundle#begin/end.
" plugin on GitHub repo
Plugin 'tpope/vim-fugitive'
" Surround
Plugin 'tpope/vim-surround'
" Repeat
Plugin 'tpope/vim-repeat'
" Hardtime
Plugin 'takac/vim-hardtime'
" plugin from http://vim-scripts.org/vim/scripts.html
Plugin 'L9'
" Gitgutter
Plugin 'airblade/vim-gitgutter'
" EasyMotion
"Plugin 'Lokaltog/vim-easymotion'
" Tags
Plugin 'xolox/vim-misc'
Plugin 'xolox/vim-easytags'
" Speed dating (increment dates)
Plugin 'tpope/vim-speeddating'
" NERD Tree
Plugin 'scrooloose/nerdtree'
" NERD Commenter
Plugin 'scrooloose/nerdcommenter'
" Tagbar
Plugin 'majutsushi/tagbar'
" Status bar
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'
Plugin 'bling/vim-bufferline'
" Syntax checking
Plugin 'scrooloose/syntastic'
" Virtualenv
Plugin 'jmcantrell/vim-virtualenv'
" CtrlP (fuzzy file finder)
Plugin 'kien/ctrlp.vim'
" Autocompletion
" Plugin 'Valloric/YouCompleteMe'
" Base16 theme
Plugin 'chriskempson/base16-vim'
" Tmux navigation
Plugin 'christoomey/vim-tmux-navigator'
" Go
Plugin 'fatih/vim-go'
" Multiple Cursors
Plugin 'terryma/vim-multiple-cursors'
" dbext
Plugin 'vim-scripts/dbext.vim'
" Python Mode
Plugin 'klen/python-mode'
" rust.vim
Plugin 'rust-lang/rust.vim'
" vim-expand-region
Plugin 'terryma/vim-expand-region'
" Targets.vim
Plugin 'wellle/targets.vim'
" tslime.vim
Plugin 'jgdavey/tslime.vim'
" git repos on your local machine (i.e. when working on your own plugin)
" Plugin 'file:///home/gmarik/path/to/plugin'
" The sparkup vim script is in a subdirectory of this repo called vim.
" Pass the path to set the runtimepath properly.
" Plugin 'rstacruz/sparkup', {'rtp': 'vim/'}
" Avoid a name conflict with L9
" Plugin 'user/L9', {'name': 'newL9'}
" Nice undo tree
Plugin 'mbbill/undotree'
" Fuzzy find
Plugin 'junegunn/fzf.vim'

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line

syntax on

let mapleader = "\<Space>"
nnoremap <Leader>w :w<CR>
vmap <Leader>y "+y
vmap <Leader>d "+d
nmap <Leader>p "+p
nmap <Leader>P "+P
vmap <Leader>p "+p
vmap <Leader>P "+P

" Quick switching of buffers
map gn :bn<cr>
map gp :bp<cr>
map gd :bd<cr>

" Stop weird q window popping up
map q: :q

" let g:EasyMotion_do_mapping = 0 " Disable default EasyMotion mappings

" nmap s <Plug>(easymotion-s2)
" map / <Plug>(easymotion-sn)
" omap / <Plug>(easymotion-tn)
"
" map <leader>l <Plug>(easymotion-lineforward)
" map <leader>j <Plug>(easymotion-j)
" map <leader>k <Plug>(easymotion-k)
" map <leader>h <Plug>(easymotion-linebackward)
" let g:EasyMotion_startofline = 0

" Syntastic things
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 0
let g:syntastic_check_on_wq = 0

" Vim Airline things
set laststatus=2
let g:airline#extensions#tabline#enabled = 1
let g:airline_powerline_fonts = 1
let g:airline_theme='luna'

" Tagbar shortcut
nmap <F8> :TagbarToggle<CR>

" Colour scheme
"let base16colorspace=256 " Access colors present in 256 colorspace
set background=light
colorscheme base16-default-dark

" dbext Profiles
" Redshift
let g:dbext_default_profile_redshift = 'type=PGSQL:user=$REDSHIFT_USERNAME:passwd=$REDSHIFT_PASSWORD:dbname=dev:host=diu-redshift.cdm5jijpmsyq.eu-west-1.redshift.amazonaws.com:port=5439'

" Make a simple "search" text object.
vnoremap <silent> s //e<C-r>=&selection=='exclusive'?'+1':''<CR><CR>
    \:<C-u>call histdel('search',-1)<Bar>let @/=histget('search',-1)<CR>gv
omap s :normal vs<CR>

" tslime
vmap <Leader>r <Plug>SendSelectionToTmux
nmap <Leader>r <Plug>NormalModeSendToTmux
nmap <Leader>t <Plug>SetTmuxVars
let g:tslime_always_current_session = 1
let g:tslime_always_current_window = 1

" Allow F2 to be used for pasting
set pastetoggle=<F2>

" Open the undo tree
nnoremap <F5> :UndotreeToggle<cr>

